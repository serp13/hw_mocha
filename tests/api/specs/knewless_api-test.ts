import chai, { expect } from "chai"
import jsonSchema from 'chai-json-schema'
chai.use(jsonSchema)
import schemas from './data/schemas_knewlessData.json'
import testData from './data/add_data.json'
//import invData from './data/add_data.json'
import { AuthController } from "../lib/controllers/auth.controller"
import { UserController } from "../lib/controllers/user.controller"
import { AuthorController } from "../lib/controllers/author.controller"
import { StudentController } from "../lib/controllers/student.controller"
import { GoalsController } from "../lib/controllers/progress-goal.controller"
import { ArticleController } from "../lib/controllers/article.controller"
import { FavoriteController } from "../lib/controllers/favorite.controller"
import { ArticleCommentController } from "../lib/controllers/article-comment.controller"
import { checkResponseTime, checkStatusCode, checkResponseBodyMessage, checkResponseBodyStatus, checkResponseBodySchema,
    checkResponseBodyID, checkResponseBodyText, checkResponseBodyName, checkResponseBodyEmail} 
        from '../../helpers/functionsForChecking.helper'
const authCtrl = new AuthController();
const userCtrl = new UserController();
const authorCtrl = new AuthorController();
const studentCtrl = new StudentController();
const articleCtrl = new ArticleController();
const goalsCtrl = new GoalsController();
const favoriteCtrl = new FavoriteController();
const commentArticleCtrl = new ArticleCommentController();
 
describe("AUTHOR role | with hooks", () => {
    const authorUserData = global.appConfig.users.authorGood;
    const authorTestData = testData.authorGood;
    let accessToken: string;
    let idAuthor: string;
    let idUser: string;
    let idArticle: string;
   
    before(`userLogin`, async () => {
        let response = await authCtrl.userLogin(authorUserData.email, authorUserData.password);
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_login)
        accessToken = response.body.accessToken;
    });

    it(`getDataAuthor`, async () => {
        let response = await authorCtrl.getDataAuthor(accessToken);
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_getAuthor);
        idAuthor = response.body.id;
        idUser = response.body.userId;
    });

    it(`updateDataAuthor`, async () => {
        let response = await authorCtrl.updateDataAuthor(accessToken, idAuthor, authorTestData.newFirstName, 
            authorTestData.newLastName, authorTestData.location);
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_postAuthor);
    });
    
    it(`getDataUser`, async () => {
        let response = await userCtrl.getDataUser(accessToken);
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_userMe);
        checkResponseBodyID(response, idUser)
        checkResponseBodyEmail(response, authorUserData.email)  
        expect(response.body.role.name, `User role should be AUTHOR`).to.be.equal("AUTHOR");     
    });

    it(`getAuthorOverview`, async () => {
        let response = await authorCtrl.getAuthorOverview(accessToken, idAuthor);
       checkStatusCode(response,200);
       checkResponseTime(response);
       checkResponseBodySchema(response, schemas.schema_authorOverview);
       checkResponseBodyID(response, idAuthor);
       expect(response.body.userId, `User ID check`).to.be.equal(idUser);
       expect(response.body.firstName, `Author First Name check`).to.be.equal(authorTestData.newFirstName);
       expect(response.body.lastName, `Author Second Name check`).to.be.equal(authorTestData.newLastName);
    });

    it(`createNewArticle`, async () => {
        let response = await articleCtrl.createNewArticle(accessToken, idAuthor, authorTestData.newFirstName+' '+authorTestData.newLastName, 
        authorTestData.image, authorTestData.title, authorTestData.text);
        checkResponseBodySchema(response, schemas.schema_article);
        checkStatusCode(response,200);
        checkResponseTime(response);    
        checkResponseBodyName(response, authorTestData.title)
        checkResponseBodyText(response, authorTestData.text)
        expect(response.body.image, `Author avatar check`).to.be.equal(authorTestData.image);
        expect(response.body.authorId, `Author ID check`).to.be.equal(idAuthor);  
        expect(response.body.authorName, `Author Name check`).to.be.equal(authorTestData.newFirstName+' '+authorTestData.newLastName);   
        idArticle = response.body.id;
    });
    
    it(`getAllArticleAuthor`, async () => {
        let response = await articleCtrl.getAllArticleAuthor(accessToken);
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_articleAuthor); 
        for (let i = 0; i < response.body.length; i++)  {// проверяем, все ли полученные Артикли принадлежат этому Автору
            expect(response.body[i].authorId, `Author ID check`).to.be.equal(idAuthor);   
        } 
    });

    it(`getArticleById`, async () => {
        let response = await articleCtrl.getArticleById(accessToken, idArticle);
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_articleId); 
        checkResponseBodyName(response, authorTestData.title)
        checkResponseBodyText(response, authorTestData.text)
        expect(response.body.image, `Article image check`).to.be.equal(authorTestData.image);
    });

    it(`createArticleComment`, async () => {
        let response = await commentArticleCtrl.createArticleComment(accessToken, idArticle, authorTestData.comment);
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_postArticleComment);  
        checkResponseBodyText(response, authorTestData.comment)
        expect(response.body.articleId, `Article ID check`).to.be.equal(idArticle);       
    }); 
    
    it(`getAllArticleComments`, async () => {
        let response = await commentArticleCtrl.getAllArticleComments(accessToken, idArticle);
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_getArticleComments);  
        for (let i = 0; i < response.body.length; i++)  { // проверяем, все ли полученные комментарии принадлежат этому Артиклю
            expect(response.body[i].articleId, `Article ID check`).to.be.equal(idArticle);   
        }        
    }); 

    afterEach(function () {
        // тут могут быть действия, необходимые после КАЖДОГО теста
    });

});

describe("STUDENT role", () => {
    const studentUserData = global.appConfig.users.studentGood;
    const studentTestData = testData.studentGood;
    let accessToken: string;
    let idGoal: string;
    let isFavorite = false;
     
    before(`userLogin`, async () => {
        let response = await authCtrl.userLogin(studentUserData.email, studentUserData.password);
        accessToken = response.body.accessToken;
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_login);
    });

    it(`getDataUser`, async () => {
        let response = await userCtrl.getDataUser(accessToken);
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_userMe);
        checkResponseBodyEmail(response, studentUserData.email)
        //expect(response.body.email, `User email check`).to.be.equal(studentUserData.email);      
        expect(response.body.role.name, `User role should be USER`).to.be.equal("USER");     
    });

    it(`getGoalsAll`, async () => {
        let response = await goalsCtrl.getGoalsAll(accessToken);
        idGoal = response.body[0].id; //берем самую первую цель из списка
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_goals);
        expect(response.body.length, `Number of goals must be > 0`).to.be.greaterThan(0);  
    });

    it(`setGoalStudent`, async () => {
        let response = await studentCtrl.setGoalStudent(accessToken, idGoal);
        checkStatusCode(response,200);
        checkResponseTime(response);
    });

    it(`getFavouriteAuthors`, async () => {
        let response = await favoriteCtrl.getFavouriteAuthors(accessToken);
        for (let i = 0; i < response.body.length; i++)  {
            isFavorite ||= response.body[i].id === studentTestData.idAuthor; //в избранном ли автор? для проверки в следующем запросе   
        } 
        checkStatusCode(response,200);
        checkResponseTime(response);
        checkResponseBodySchema(response, schemas.schema_favoriteAuthors);
    });

    it(`changeStatusFavoriteAuthor`, async () => {
        let response = await favoriteCtrl.changeStatusFavoriteAuthor(accessToken, studentTestData.idAuthor);
        expect(response.body, `Favorite status should be changed`).to.be.equal(!isFavorite); 
        checkStatusCode(response,200);
        checkResponseTime(response);
    });   
    
    afterEach(function () {
        // тут могут быть действия, необходимые после КАЖДОГО теста
    }); 
});

describe("Test Data with bad credentials", () => {
    let badLoginData = [
        { email: 'fji02maZ', password: 'waliges340@altpano.com' },
        { email: '1waliges340@altpano.com', password: ' ' },
        { email: ' ', password: 'fji02maZ' },
        { email: '1waliges340', password: 'fji02maZ' },
        { email: '1waliges340@altpano.com', password: '1waliges340@altpano.com' },
        { email: '1waliges340 @altpano.com', password: 'fji02maZ' },
        { email: 'fwaliges340@altpano.com ', password: 'fji02maZ' }
    ];

    badLoginData.forEach((cred) => {
        it(`login using invalid creds: ${cred.email} ${cred.password}`, async() => {
            let response = await authCtrl.userLogin(cred.email, cred.password);
            checkStatusCode(response,401);
            checkResponseBodyStatus(response, "UNAUTHORIZED")
            checkResponseBodyMessage(response, "Bad credentials", 'equal');
            checkResponseTime(response);
        })
    })
})

describe("NEGATIVE testing", () => {
    const email = global.appConfig.users.studentGood.email;
    const password = global.appConfig.users.studentGood.password;

    it(`regRegisteredData`, async () => { // попытка регистрации с корректными данными зарегистрированного пользователя
        let response = await authCtrl.userRegister(email, password);
        checkStatusCode(response,400);
        checkResponseTime(response);
        checkResponseBodyMessage(response, `User with email '${email}' is already registered.`, 'equal');      
    });
       
    it(`regEmailPassMixed`, async () => { // попытка логина с перепутанными местами корректными логином-паролем
        let response = await authCtrl.userLogin(password, email);
        checkStatusCode(response,401);
        checkResponseTime(response);
        checkResponseBodyStatus(response, "UNAUTHORIZED")
        checkResponseBodyMessage(response, "Bad credentials", 'equal');
    });
       
    it(`editSomeoneElseArticle`, async () => { // попытка редактировать чужой Артикль
        let response = await authCtrl.userLogin(email, password);
        response = await articleCtrl.editArticleById(response.body.accessToken, testData.studentGood.idBadArticle);
        checkStatusCode(response,404);
        checkResponseTime(response);
        checkResponseBodyStatus(response, "NOT_FOUND")
        checkResponseBodyMessage(response, "Author not found", 'contains');
    });

});

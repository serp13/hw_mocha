import { ApiRequest } from "../request";
const prefixUrl = global.appConfig.baseUrl;

export class FavoriteController {
    
    async getFavouriteAuthors(token: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("GET")
            .url(`favorite/authors`) 
            .bearerToken(token)
            .send()
        return response;
    }

    async changeStatusFavoriteAuthor(token: string, authorID: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("POST")
            .url(`favorite/change?id=${authorID}&type=AUTHOR`) 
            .bearerToken(token)
            .send()
        return response;
    }
}
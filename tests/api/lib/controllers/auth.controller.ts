import { ApiRequest } from "../request";
const prefixUrl = global.appConfig.baseUrl;

export class AuthController {

    async userRegister(email: string, password: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("POST")
            .url(`auth/signup`) 
            .body({email, password})
            .send()
        return response;
    }

    async userLogin(email: string, password: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("POST")
            .url(`auth/login`) 
            .body({email, password})
            .send()
        return response;
    }
}


import { ApiRequest } from "../request";
const prefixUrl = global.appConfig.baseUrl;

export class StudentController {

    async setGoalStudent(token: string, goalId: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("POST")
            .url(`student/goal`) 
            .bearerToken(token)
            .body({goalId})
            .send()
        return response;
    }
 }


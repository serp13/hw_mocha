import { ApiRequest } from "../request";
const prefixUrl = global.appConfig.baseUrl;

export class ArticleCommentController {
    
    async createArticleComment(token: string, articleId: string, text: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("POST")
            .url(`article_comment`) 
            .bearerToken(token)
            .body({articleId, text})
            .send()
        return response;
    }
    
    async getAllArticleComments(token: string, articleId: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("GET")
            .url(`article_comment/of/${articleId}?size=200`) 
            .bearerToken(token)
            .send()
        return response;
    }
}
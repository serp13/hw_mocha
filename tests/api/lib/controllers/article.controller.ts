import { ApiRequest } from "../request";
const prefixUrl = global.appConfig.baseUrl;

export class ArticleController {
    
    async createNewArticle(token: string, authorId: string, authorName: string, image: string, name: string, text: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("POST")
            .url(`article`) 
            .bearerToken(token)
            .body({authorId, authorName, image, name, text})
            .send()
        return response;
    }

    async getAllArticleAuthor(token: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("GET")
            .url(`article/author`) 
            .bearerToken(token)
            .send()
        return response;
    }
    
    async getArticleById(token: string, articleId: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("GET")
            .url(`article/${articleId}`) 
            .bearerToken(token)
            .send()
        return response;
    }
    
    async editArticleById(token: string, articleId: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("GET")
            .url(`article/${articleId}/edit`) 
            .bearerToken(token)
            .send()
        return response;
    }
}
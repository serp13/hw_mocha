import { ApiRequest } from "../request";
const prefixUrl = global.appConfig.baseUrl;

export class AuthorController {

    async getDataAuthor(token: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("GET")
            .url(`author`) 
            .bearerToken(token)
            .send()
        return response;
    }

    async updateDataAuthor(token: string, id: string, firstName: string, lastName: string, location: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("POST")
            .url(`author`) 
            .bearerToken(token)
            .body({id, firstName, lastName, location})
            .send()
        return response;
    }
  
    async getAuthorOverview(token: string, authorId: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("GET")
            .url(`author/overview/${authorId}`) 
            .bearerToken(token)
            .send()
        return response;
    }
    
}


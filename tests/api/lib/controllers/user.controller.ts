import { ApiRequest } from "../request";
const prefixUrl = global.appConfig.baseUrl;

export class UserController {
 
    async getDataUser(token: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("GET")
            .url(`user/me`) 
            .bearerToken(token)
            .send()
        return response;
    }
}


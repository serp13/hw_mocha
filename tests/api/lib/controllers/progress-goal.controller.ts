import { ApiRequest } from '../request';
let prefixUrl: string = global.appConfig.baseUrl;

export class GoalsController {

    async getGoalsAll(token: string) {
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method("GET")
            .url(`goals`) 
            .bearerToken(token)
            .send()
        return response;
    }

}
import { expect } from 'chai';

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseBodySchema(response, schema) {
    expect(response.body, `Response should be according to the schema`).to.be.jsonSchema(schema);
}

export function checkResponseBodyStatus(response, status: string) {
    expect(response.body.status, `Status should be ${status}`).to.be.equal(status);
}

export function checkResponseBodyName(response, name: string) {
    expect(response.body.name, `Name should be ${name}`).to.be.equal(name);
}

export function checkResponseBodyID(response, id: string) {
    expect(response.body.id, `ID should be ${id}`).to.be.equal(id);
}

export function checkResponseBodyEmail(response, email: string) {
    expect(response.body.email, `Email should be ${email}`).to.be.equal(email);
}

export function checkResponseBodyText(response, text: string) {
    expect(response.body.text, `Text should be ${text}`).to.be.equal(text);
}


export function checkResponseBodyMessage(response, message: string, type: 'equal' | 'contains') {
    switch (type) {
        case 'contains': {
            expect(response.body.message, `Message should be ${message}`).contains(message);
            break;
        }
        case 'equal': {
            expect(response.body.message, `Message should be ${message}`).to.be.equal(message);
            break;
        }
    }
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );
}
